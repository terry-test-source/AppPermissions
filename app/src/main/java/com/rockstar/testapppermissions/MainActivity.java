package com.rockstar.testapppermissions;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import static android.Manifest.permission.READ_SMS;

public class MainActivity extends AppCompatActivity {

    public static final int READ_SMS_REQUEST = 210;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Presenter.mayRequestPermissions(
                this,
                this,
                READ_SMS,READ_SMS_REQUEST,
                "Se necesitan permisos de contactos para autocompletado");

    }
    
}
