package com.rockstar.testapppermissions;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;

/**
 * TestAppPermissions
 * Created by terry0022 on 09/01/17 - 13:17.
 */
public class Presenter {


    public Presenter() {
        // Empty Constructor
    }

    /**
     *  <strong>Verificar los permisos de la aplicación</strong>
     *  <p>Se pregunta la version del dispositivo si es M o 23<br/>
     *  Se pregunta si los permisos han sido otorgados ya<br/>
     *
     *  @param activity Actividad en donde se manean los permisos.
     *  @param permission permiso proveniente desde {@link android.Manifest.permission }.
     *  @param requestCode el codigo que devolverá la funcion llamada {@link Activity#requestPermissions(String[], int)}
     *  @param text texto a mostrar en el snackbar.
     *
     *  @return <i>True</i> <p>Si no es android M, si los permisos fueron anterioramente otorgados</p><br/>
     *  <i>False</i> <p>Al mostrar el Snackbar</p>
     *
     *  @see android.os.Build.VERSION
     *  @see android.os.Build.VERSION_CODES
     *  @see Activity#checkSelfPermission(String) */
    public static boolean mayRequestPermissions(final Context context,
                                                final Activity activity,
                                                final String permission,
                                                final int requestCode,
                                                final String text) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (activity.shouldShowRequestPermissionRationale(permission)) {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle("Permisos");
            alert.setMessage(text);
            alert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                @TargetApi(Build.VERSION_CODES.M)
                public void onClick(DialogInterface dialog, int which) {
                    activity.requestPermissions(new String[]{permission}, requestCode);
                }
            });
            alert.show();
        } else {
            activity.requestPermissions(new String[]{permission}, requestCode);
        }
        return false;
    }


}
